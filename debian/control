Source: napalm-fortios
Section: python
Priority: optional
Maintainer: Vincent Bernat <bernat@debian.org>
Uploaders: Debian Python Team <team+python@tracker.debian.org>
Build-Depends: debhelper-compat (= 9), dh-python,
               python-all,
               python-setuptools,
               python-pip,
               python-napalm-base,
               python-pyfg
Standards-Version: 4.1.1
Homepage: https://github.com/napalm-automation/napalm-fortios
Vcs-Git: https://salsa.debian.org/python-team/packages/napalm-fortios.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/napalm-fortios

Package: python-napalm-fortios
Architecture: all
Depends: ${misc:Depends}, ${python:Depends},
Recommends: ${python:Recommends}
Suggests: ${python:Suggests}
XB-Python-Egg-Name: napalm-fortios
Description: abstraction layer for multivendor network automation - FortiOS support
 NAPALM (Network Automation and Programmability Abstraction Layer with
 Multivendor support) is a Python library that implements a set of
 functions to interact with different router vendor devices using a
 unified API.
 .
 NAPALM supports several methods to connect to the devices, to
 manipulate configurations or to retrieve data.
 .
 This module provides support for Fortinet FortiOS-based devices.
